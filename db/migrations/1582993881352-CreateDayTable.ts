import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey} from "typeorm";

export class CreateDayTable1582993881352 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.createTable(new Table({
            name: "day",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                },
                {
                    name: "title",
                    type: "varchar(100)",
                }
            ]
        }));

        await queryRunner.addColumn(
            "page",
            new TableColumn({
                name: "day_id",
                type: "int",
            }),
        )

        await queryRunner.createForeignKey("page", new TableForeignKey({
            columnNames: ["day_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "day",
            onDelete: "CASCADE",
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {

        const table = await queryRunner.getTable("page");

        await queryRunner.dropForeignKey(table, table.foreignKeys[0]);

        await queryRunner.dropColumn("page", "day_id");

        await queryRunner.dropTable("day");
    }

}
