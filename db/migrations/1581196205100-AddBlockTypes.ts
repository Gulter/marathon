import {MigrationInterface, QueryRunner, getRepository, getConnection} from "typeorm";
import { BlockType } from "../entities/BlockType";
import { BlockTypeEnum } from "../types/BlockTypeEnum";

export class AddBlockTypes1581196205100 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(BlockType)
            .values([
                {
                    name: "Text",
                    id: BlockTypeEnum.TEXT
                },
                {
                    name: "Picture",
                    id: BlockTypeEnum.PICTURE
                },
                {
                    name: "Link",
                    id: BlockTypeEnum.LINK
                },
                {
                    name: "Video",
                    id: BlockTypeEnum.VIDEO
                },
                {
                    name: "Audio",
                    id: BlockTypeEnum.AUDIO
                },
            ])
            .execute();
    }

    public async down(queryRunner: QueryRunner): Promise<any> {

        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(BlockType)
            .where("id = any (array(SELECT unnest(array[1, 2, 3, 4, 5])))")
            .execute();
    }

}
