import {MigrationInterface, QueryRunner, getConnection} from "typeorm";
import { BlockType } from "../entities/BlockType";
import { BlockTypeEnum } from "../types/BlockTypeEnum";

export class AddTextareaBlockType1583052619454 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(BlockType)
            .values([
                {
                    name: "Textarea",
                    id: BlockTypeEnum.TEXTAREA,
                },
            ])
            .execute();
    }

    public async down(queryRunner: QueryRunner): Promise<any> {

        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(BlockType)
            .where("id = any (array(SELECT unnest(array[6])))")
            .execute();
    }

}
