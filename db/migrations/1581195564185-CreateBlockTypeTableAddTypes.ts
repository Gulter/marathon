import {MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateBlockTypeTableAddTypes1581195564185 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.createTable(new Table({
            name: "block_type",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                },
                {
                    name: "name",
                    type: "varchar(100)",
                }
            ]
        }), true);

    }

    public async down(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.dropTable("block_type");
    }
}
