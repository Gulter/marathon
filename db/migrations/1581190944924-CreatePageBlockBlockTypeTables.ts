import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreatePageBlockBlockTypeTables1581190944924 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.createTable(new Table({
            name: "page",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                },
                {
                    name: "title",
                    type: "varchar(100)",
                }
            ]
        }), true);

        await queryRunner.createTable(new Table({
            name: "block",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                },
                {
                    name: "text",
                    type: "text",
                    isNullable: true,
                },
                {
                    name: "url",
                    type: "varchar(250)",
                    isNullable: true,
                },
                {
                    name: "block_type",
                    type: "int",
                },
                {
                    name: "priority",
                    type: "int",
                },
                {
                    name: "page_id",
                    type: "int",
                }
            ]
        }), true);

        await queryRunner.createForeignKey("block", new TableForeignKey({
            columnNames: ["page_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "page",
            onDelete: "CASCADE",
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.dropTable("block");

        await queryRunner.dropTable("page");
    }
}
