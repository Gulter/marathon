import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn } from "typeorm";
import { Block } from "./Block";
import { Day } from "./Day";

@Entity()
export class Page {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: 100,
    })
    title: string;

    @OneToMany(type => Block, block => block.page)
    blocks: Block[];

    @ManyToOne(type => Day, day => day.pages)
    @JoinColumn({ name: "day_id" })
    day: Day;

    @Column({ name: "day_id" })
    dayId: number;
}