import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { BlockTypeEnum } from "../types/BlockTypeEnum";
import { Page } from "./Page";

@Entity()
export class Block {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    text: string;

    @Column({ nullable: true })
    url: string;

    @Column({ name: "block_type" })
    blockType: BlockTypeEnum;

    @Column()
    priority: number;

    @ManyToOne(type => Page, page => page.blocks)
    @JoinColumn({name: "page_id"})
    page: Page;

    @Column({ name: "page_id" })
    pageId: number;
}