import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Page } from "./Page";

@Entity()
export class Day {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: 100,
    })
    title: string;

    @OneToMany(type => Page, page => page.day)
    pages: Page[];
}