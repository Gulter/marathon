import { Entity, Column, PrimaryColumn } from "typeorm";

@Entity()
export class BlockType {

    @PrimaryColumn()
    id: number;

    @Column({
        length: 100,
    })
    name: string;
}