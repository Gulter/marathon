import App from "./app"

import * as bodyParser from "body-parser"
import loggerMiddleware from "./middleware/logger"
import { HomeController } from "./controllers/home.controller"
import { UsersController } from "./controllers/users.controller"
import { createConnection } from "typeorm";
import { AdminController } from "./controllers/admin.controller"

createConnection().then(() => {
    const app = new App({
        port: 5000,
        controllers: [
            new HomeController(),
            new UsersController(),
            new AdminController(),
        ],
        middleWares: [
            bodyParser.json(),
            bodyParser.urlencoded({ extended: true }),
            loggerMiddleware
        ]
    });

    app.listen();
}).catch(error => console.log(error));
