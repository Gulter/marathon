import * as E from "linq";
import { Page } from "../../db/entities/Page";
import { getRepository } from "typeorm";
import { Day } from "../../db/entities/Day";
import { Block } from "../../db/entities/Block";

export class AdminService {

    private pageRepository = getRepository(Page);
    private dayRepository = getRepository(Day);
    private blockRepository = getRepository(Block);

    constructor() {}

    async getAll() {

        return this.pageRepository.find({
            relations: ["blocks"],
        });
    }

    async getOne(id: number) {

        return this.pageRepository.findOne(id, {
            relations: ["blocks"],
        })
    }

    async savePage(page: Page) {

        const newPage = this.pageRepository.create(page);

        return this.pageRepository.save(newPage);
    }

    async daysGetAll() {

        return this.dayRepository.find({
            relations: ["pages", "pages.blocks"]
        })
    }

    async saveDay(day: Day) {

        const newDay = this.dayRepository.create(day);

        return this.dayRepository.save(newDay);
    }

    async saveBlock(block: Block) {


        const priorities = await this.getMaxBlockPriority(block.pageId);

        block.priority = priorities.length
            ? E.from(priorities).select(i => i.priority).max() + 1
            : 1;

        const newBlock = this.blockRepository.create(block);

        return this.blockRepository.save(newBlock);
    }

    private async getMaxBlockPriority(pageId: number) {

        return this.blockRepository.find({
            select: ["priority"],
            where: {
                pageId,
            }
        })
    }

    async deleteBlock(blockId: number) {

        const block = await this.blockRepository.findOne(blockId);

        try {

            const res = await this.blockRepository.delete(blockId);

        } catch (error) {

            return "error";
        }


        await this.updateBlocksPriority(block);

        return "OK";
    }

    private async updateBlocksPriority(block: Block) {

        return this.blockRepository
            .query(`
                UPDATE block
                SET priority = priority - 1
                WHERE page_id = ${block.pageId} AND priority > ${block.priority}
            `)
    }

    async changePriorityWith(id: number, otherId: number) {

        const blocks = await this.blockRepository.find({
            where: `id = ${id} OR id = ${otherId}`
        });

        try {

            const priority = blocks[0].priority;
            blocks[0].priority = blocks[1].priority;
            blocks[1].priority = priority;

            await this.blockRepository.save(blocks);
        } catch(error) {

            return "error"
        }

        return "OK";
    }
}