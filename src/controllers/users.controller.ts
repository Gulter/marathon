import * as express from "express"
import { Request, Response } from "express"
import { IControllerBase } from "./IControllerBase.interface"

export class UsersController implements IControllerBase {
    public path = "/users"
    public router = express.Router()

    constructor() {
        this.initRoutes()
    }

    public initRoutes() {
        this.router.get(this.path, this.getAllUsers)
    }

    getAllUsers = (req: Request, res: Response) => {

        const users = [
            {
                id: 1,
                name: "Ali"
            },
            {
                id: 2,
                name: "Can"
            },
            {
                id: 3,
                name: "Ahmet"
            }
        ]

        res.json(users);
    }
}
