import * as express from "express";
import { Request, Response } from "express";
import { IControllerBase } from "./IControllerBase.interface";
import { AdminService } from "../services/admin.service";
import { Day } from "../../db/entities/Day";
import { Page } from "../../db/entities/Page";
import { Block } from "../../db/entities/Block";

export class AdminController implements IControllerBase {
    public path = "/admin"
    public router = express.Router()
    private adminService = new AdminService();

    constructor() {
        this.initRoutes()
    }

    public initRoutes() {
        this.router.get(this.path + "/days", this.getDays);
        this.router.post(this.path + "/days", this.saveDay);
        this.router.get(this.path + "/pages", this.getPages);
        this.router.get(this.path + "/pages/:id", this.getPage);
        this.router.post(this.path + "/pages", this.savePage);
        this.router.post(this.path + "/blocks", this.saveBlock);
        this.router.delete(this.path + "/blocks/:id", this.deleteBlock);
        this.router.get(this.path + "/blocks/:id/changePriorityWith/:otherId", this.changePriorityWith);
    }

    getDays = async (req: Request, res: Response) => {

        const days = await this.adminService.daysGetAll();

        res.json(days);
    }

    saveDay = async (req: Request, res: Response) => {

        const day: Day = req.body;

        const newDay = await this.adminService.saveDay(day);
        res.json(newDay);
    }

    getPages = async (req: Request, res: Response) => {

        const pages = await this.adminService.getAll();

        res.json(pages);
    }

    getPage = async (req: Request, res: Response) => {

        const id = +req.params.id;

        const page = await this.adminService.getOne(id);

        if (!page) {
            res.status(404).send({
                "error": "Post not found!"
            })
        }

        res.json(page);
    }

    savePage = async (req: Request, res: Response) => {

        const page: Page = req.body;

        const newPage = await this.adminService.savePage(page);
        res.json(newPage);
    }

    saveBlock = async (req: Request, res: Response) => {

        const block: Block = req.body;

        const newBlock = await this.adminService.saveBlock(block);
        res.json(newBlock);
    }

    deleteBlock = async (req: Request, res: Response) => {

        const id = +req.params.id;

        const result = await this.adminService.deleteBlock(id);

        res.json(result);
    }

    changePriorityWith = async (req: Request, res: Response) => {

        const id = +req.params.id;
        const otherId = +req.params.otherId;

        const result = await this.adminService.changePriorityWith(id, otherId);

        res.json(result);
    }
}
