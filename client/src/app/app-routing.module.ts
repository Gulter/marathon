import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminPagesComponent } from "./admin/admin-pages/admin-pages.component";
import { AdminPageComponent } from "./admin/admin-page/admin-page.component";
import { DaysViewComponent } from "./days-view/days-view.component";

const routes: Routes = [
    {
        path: "",
        component: DaysViewComponent,
    },
    {
        path: "admin/pages",
        component: AdminPagesComponent,
    },
    {
        path: "admin/pages/:id",
        component: AdminPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
