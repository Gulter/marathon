import { Component, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "app-navigation",
    templateUrl: "./navigation.component.html",
    styleUrls: ["./navigation.component.scss"],
})
export class NavigationComponent {

    @Output() prevDay = new EventEmitter();
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();
    @Output() nextDay = new EventEmitter();
}
