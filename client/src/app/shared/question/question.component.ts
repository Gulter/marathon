import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "app-question",
    templateUrl: "./question.component.html",
    styleUrls: ["./question.component.scss"],
})
export class QuestionComponent {

    @Input() name: string;
    @Input() question: string;
    @Input() answer: boolean;
    @Output() answerChange = new EventEmitter<boolean>();

    get idYes() { return `${this.name}_yes`; }
    get idNo() { return `${this.name}_no`; }

    onAnswerChange(model: boolean){

        this.answer = model;
        this.answerChange.emit(model);
    }
}
