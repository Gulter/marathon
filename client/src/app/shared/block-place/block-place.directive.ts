import { Directive, OnInit, Attribute, Input, ElementRef, Renderer2, HostListener } from "@angular/core";

@Directive({
    selector: "[block-place]",
})
export class BlockPlaceDirective implements OnInit {

    @Input() blockId: number;

    refElement: any;

    constructor(
        private element: ElementRef,
        private renderer: Renderer2,
    ) {}

    ngOnInit() {

        setTimeout(() => {

            this.refElement = document.getElementById(`block-id-${this.blockId}`);

            const refElementOffsetTop = this.refElement.offsetTop;
            const elementOffsetTop = this.element.nativeElement.offsetTop;

            const value = refElementOffsetTop - elementOffsetTop;

            if (value > 0) {

                this.renderer.setStyle(this.element.nativeElement, "margin-top", `${value}px`);
            }
        }, 1000);
    }

    @HostListener("mouseover")
    onMouseover() {

        this.renderer.addClass(this.refElement, "block-moved");
    }

    @HostListener("mouseout")
    onMouseout() {

        this.renderer.removeClass(this.refElement, "block-moved");
    }
}
