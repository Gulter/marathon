import { Component, Input } from "@angular/core";

@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.scss"],
})
export class HeaderComponent {

    @Input() day: number;

    get subCaption() {

        const map = {
            [9]: "подзаголовок для девятого дня",
            [11]: "подзаголовок для одиннадцатого дня",
        };

        return map[this.day];
    }

    get suffix() {

        const day = this.day;

        return day % 2 == 0
            ? `${day - 1}_${day}`
            : `${day}_${day + 1}`;
    }

    get dayName() {

        const dayNames = [
            "первый",
            "второй",
            "третий",
            "червертый",
            "пятый",
            "шестой",
            "седьмой",
            "восьмой",
            "девятый",
            "десятый",
            "одиннадцатый",
            "двенадцатый",
            "тринадцатый",
            "четырнадцатый",
            "пятнадцатый",
            "шестнадцатый",
            "семнадцатый",
            "восемнадцатый",
            "девятнадцатый",
            "двадцатый",
            "двадцать первый",
        ];

        const day = this.day;
        const length = dayNames.length;

        return (day >= 1 && day <= length)
            ? dayNames[day - 1]
            : day;
    }
}
