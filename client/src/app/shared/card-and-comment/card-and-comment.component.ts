import * as E from "linq";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Card } from "../../state";

@Component({
    selector: "app-card-and-comment",
    templateUrl: "./card-and-comment.component.html",
    styleUrls: ["./card-and-comment.component.scss"],
})
export class CardAndCommentComponent implements OnInit {

    @Input() id: string;
    @Input() cardNumber: number;
    @Input() cardCanBeSelected: boolean;
    @Input() cards: Card[];
    @Input() selectedCards: Card[];
    @Input() note: string;
    @Output() noteChange = new EventEmitter<string>();

    card: Card;

    ngOnInit() {

        this.card = E.from(this.cards)
            .where(i => i.cardNumber == this.cardNumber)
            .firstOrDefault(null, {} as Card)
    }

    onNoteChange(model: string){

        this.note = model;
        this.noteChange.emit(model);
    }
}
