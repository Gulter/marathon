import * as E from "linq";
import { Component, Input, OnInit } from "@angular/core";
import { Card } from "../../state";

@Component({
    selector: "app-card",
    templateUrl: "./card.component.html",
    styleUrls: ["./card.component.scss"],
})
export class CardComponent implements OnInit {

    @Input() cardNumber: number;
    @Input() canBeSelected: boolean;
    @Input() cards: Card[];
    @Input() selectedCards: Card[];

    get src() {

        return this.card.turned
            ? `assets/img/day${this.card.day}/page${this.card.page}/cards/card${this.card.day}_${this.card.cardNumber}.jpg`
            : "";
    }

    card: Card;

    ngOnInit() {

        this.card = E.from(this.cards)
            .where(i => i.cardNumber == this.cardNumber)
            .firstOrDefault(null, { cardNumber: this.cardNumber } as Card)
    }

    select() {

        if (this.canBeSelected) {

            this.card.clicked = true;
            this.card.turned = true;

            if (!this.card.selected) {

                this.card.selected = true;
                this.selectedCards.push(this.card);
            }
        }
    }
}
