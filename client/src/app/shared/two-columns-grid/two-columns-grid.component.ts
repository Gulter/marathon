import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-two-columns-grid",
  templateUrl: "./two-columns-grid.component.html",
  styleUrls: ["./two-columns-grid.component.scss"]
})
export class TwoColumnsGridComponent implements OnInit {

    isEditColumnShown = false;
    isAllColumnsShown = true;
    isSiteColumnShown = false;

    constructor() { }

    ngOnInit() {
    }

    showEditColumn() {

        this.isEditColumnShown = true;
        this.isAllColumnsShown = false;
        this.isSiteColumnShown = false;
    }

    showAllColumns() {

        this.isEditColumnShown = false;
        this.isAllColumnsShown = true;
        this.isSiteColumnShown = false;
    }

    showSiteColumn() {

        this.isEditColumnShown = false;
        this.isAllColumnsShown = false;
        this.isSiteColumnShown = true;
    }
}
