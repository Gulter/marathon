import { createAction, props } from "@ngrx/store";
import { Page } from "src/app/services/admin/Page";
import { Day } from "src/app/services/admin/Day";
import { Block } from 'src/app/services/admin/Block';

export const GetDaysAction = createAction(
    "[Admin] GetDays"
);

export const GetDaysSucceedAction = createAction(
    "[Admin] GetDaysSucceed",
    props<{
        days: Day[]
    }>(),
);

export const AddDayAction = createAction(
    "[Admin] Add Day"
);

export const GetPageAction = createAction(
    "[Admin] GetPage",
    props<{
        id: number
    }>()
);

export const GetPageSucceedAction = createAction(
    "[Action] GetPageSucceed",
    props<{
        page: Page
    }>()
);

export const AddPageAction = createAction(
    "[Admin] Add Page",
    props<{ day: Day }>()
);

export const AddBlockAction = createAction(
    "[Admin] Add Block",
    props<{ block: Block }>()
);

export const RemoveBlockAction = createAction(
    "[Admin] Remove Block",
    props<{ block: Block }>(),
);

export interface BlocksChangePriorityWithInterface {
    pageId: number;
    id: number;
    otherId: number;
}

export const BlocksChangePriorityWithAction = createAction(
    "[Admin] Blocks Change Priority With",
    props<BlocksChangePriorityWithInterface>(),
)