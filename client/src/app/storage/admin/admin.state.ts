import { Page } from "src/app/services/admin/Page";
import { Day } from "src/app/services/admin/Day";

export class AdminState {

    days: Day[];
    selectedPage: Page;
}