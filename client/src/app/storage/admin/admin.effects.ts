import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import {
    GetDaysAction,
    GetDaysSucceedAction,
    GetPageAction,
    GetPageSucceedAction,
    AddDayAction,
    AddPageAction,
    AddBlockAction,
    RemoveBlockAction,
    BlocksChangePriorityWithAction
} from "./admin.actions";
import { switchMap, map, catchError, filter } from "rxjs/operators";
import { Page } from "src/app/services/admin/Page";
import { of, from } from "rxjs";
import { Day } from "src/app/services/admin/Day";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AddDayModalComponent } from "src/app/admin/admin-pages/add-day-modal/add-day-modal.component";
import { AddPageModalComponent } from "src/app/admin/admin-pages/add-page-modal/add-page-modal.component";
import { Block } from "src/app/services/admin/Block";
import { DialogService } from 'src/app/services/dialog-services/confirm';

@Injectable()
export class AdminEffects {
    constructor(
        private http: HttpClient,
        private actions$: Actions,
        private modal: NgbModal,
        private dialogService: DialogService,
    ) {}

    getDays$ = createEffect(() =>
        this.actions$.pipe(
            ofType(GetDaysAction),
            switchMap(action =>
                this.http.get("http://localhost:5000/admin/days").pipe(
                    map((r: any) => r as Day[]),
                    switchMap(result =>
                        of(GetDaysSucceedAction({ days: result }))
                    )
                )
            )
        )
    );

    addDay$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AddDayAction),
            switchMap(() => {

                const modal = this.modal.open(AddDayModalComponent, {
                    size: "sm"
                });

                return from(modal.result).pipe(
                    catchError(() => of(false))
                );
            }),
            filter(i => i),
            switchMap((res) => {

                return this.http.post("http://localhost:5000/admin/days", res).pipe(
                    map((r: any) => r as Day[]),
                    switchMap(result => of(GetDaysAction()))
                );
            })
        )
    );

    addPage$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AddPageAction),
            switchMap(action => {

                const modal = this.modal.open(AddPageModalComponent, {
                    size: "sm"
                });

                modal.componentInstance.day = action.day;

                return from(modal.result).pipe(
                    catchError(() => of(false))
                );
            }),
            filter(i => i),
            switchMap((res) =>
                this.http.post("http://localhost:5000/admin/pages", res).pipe(
                    map((r: any) => r as PageTransitionEvent),
                    switchMap(result => of(GetDaysAction()))
                )
            )
        )
    );

    addBlock$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AddBlockAction),
            switchMap(action =>
                this.http.post("http://localhost:5000/admin/blocks", action.block).pipe(
                    map((r: any) => r as Block),
                    switchMap(block => {
                        return of(GetPageAction({ id: block.pageId }));
                    })
                )
            )
        )
    );

    removeBlock$ = createEffect(() =>
        this.actions$.pipe(
            ofType(RemoveBlockAction),
            switchMap(action =>
                from(this.dialogService.confirm$({
                    text: `Are you sure delete block with id ${action.block.id}`,
                })).pipe(
                    filter(result => result),
                    switchMap(() =>
                        this.http.delete(`http://localhost:5000/admin/blocks/${action.block.id}`)
                            .pipe(
                                map((r: any) => r as Page),
                                switchMap(result =>
                                    of(GetPageAction({ id: action.block.pageId }))
                                )
                            )
                    )
                )
            )
        )
    );

    BlocksChangePriorityWith$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BlocksChangePriorityWithAction),
            switchMap(action =>
                from(this.dialogService.confirm$({
                    text: `Are you sure change priorities of blocks with ids ${action.id} and ${action.otherId}`,
                })).pipe(
                    filter(result => result),
                    switchMap(() =>
                        this.http.get(`http://localhost:5000/admin/blocks/${action.id}/changePriorityWith/${action.otherId}`)
                            .pipe(
                                map((r: any) => r as Page),
                                switchMap(result =>
                                    of(GetPageAction({ id: action.pageId }))
                                )
                            )
                    )
                )
            )
        )
    )

    getPage$ = createEffect(() =>
        this.actions$.pipe(
            ofType(GetPageAction),
            switchMap(action =>
                this.http
                    .get(`http://localhost:5000/admin/pages/${action.id}`)
                    .pipe(
                        map((r: any) => r as Page),
                        switchMap(result =>
                            of(GetPageSucceedAction({ page: result }))
                        )
                    )
            )
        )
    );
}
