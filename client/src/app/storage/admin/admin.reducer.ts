import { createReducer, Action, on } from "@ngrx/store";
import { AdminState } from "./admin.state";
import { GetDaysSucceedAction, GetPageSucceedAction } from "./admin.actions";

const initialState = new AdminState();

const adminReducer = createReducer(
    initialState,
    on(GetDaysSucceedAction, (state, action) => ({ ...state, days: action.days })),
    on(GetPageSucceedAction, (state, action) => ({ ...state, selectedPage: action.page })),
);

export function AdminReducer(state: AdminState, action: Action) {

    return adminReducer(state, action);
}
