import { AdminState } from "./admin/admin.state";
import { createSelector } from "@ngrx/store";

export interface IStore {
    admin: AdminState,
}

export const getAdminState = createSelector((store:IStore) => store.admin, store => store);
