import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay2 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day2",
    templateUrl: "./day2.component.html",
    styleUrls: ["./day2.component.scss"],
})
export class Day2Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay2;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
