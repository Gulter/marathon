import * as E from "linq";
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay2 } from "../../../state";

@Component({
    selector: "app-day2-page1",
    templateUrl: "./day2-page1.component.html",
    styleUrls: ["./day2-page1.component.scss"],
})
export class Day2Page1Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay2;
    @Output() prevDay = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
