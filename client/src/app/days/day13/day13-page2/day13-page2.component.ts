import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay13 } from "../../../state";

@Component({
    selector: "app-day13-page2",
    templateUrl: "./day13-page2.component.html",
    styleUrls: ["./day13-page2.component.scss"],
})
export class Day13Page2Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay13;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
