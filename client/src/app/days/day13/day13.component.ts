import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay13 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day13",
    templateUrl: "./day13.component.html",
    styleUrls: ["./day13.component.scss"],
})
export class Day13Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay13;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
