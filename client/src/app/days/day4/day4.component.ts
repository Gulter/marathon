import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay4 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day4",
    templateUrl: "./day4.component.html",
    styleUrls: ["./day4.component.scss"],
})
export class Day4Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay4;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
