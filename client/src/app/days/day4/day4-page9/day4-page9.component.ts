import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay4 } from "../../../state";

@Component({
    selector: "app-day4-page9",
    templateUrl: "./day4-page9.component.html",
    styleUrls: ["./day4-page9.component.scss"],
})
export class Day4Page9Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay4;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
