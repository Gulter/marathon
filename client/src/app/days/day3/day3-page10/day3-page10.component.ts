import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay3 } from "../../../state";

@Component({
    selector: "app-day3-page10",
    templateUrl: "./day3-page10.component.html",
    styleUrls: ["./day3-page10.component.scss"],
})
export class Day3Page10Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay3;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    get cardCanBeSelected() { return this.state.page10.selectedCards.length < 2; }

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
