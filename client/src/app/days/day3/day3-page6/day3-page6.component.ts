import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay3 } from "../../../state";

@Component({
    selector: "app-day3-page6",
    templateUrl: "./day3-page6.component.html",
    styleUrls: ["./day3-page6.component.scss"],
})
export class Day3Page6Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay3;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    get cardCanBeSelected() { return true; }

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
