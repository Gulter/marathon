import * as E from "linq";
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay3 } from "../../../state";

@Component({
    selector: "app-day3-page2",
    templateUrl: "./day3-page2.component.html",
    styleUrls: ["./day3-page2.component.scss"],
})
export class Day3Page2Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay3;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
