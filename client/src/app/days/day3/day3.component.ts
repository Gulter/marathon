import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay3 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day3",
    templateUrl: "./day3.component.html",
    styleUrls: ["./day3.component.scss"],
})
export class Day3Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay3;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
