import { DomSanitizer } from "@angular/platform-browser";

export class BasePageComponent {

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
    }

    sanitizeTemplate(template: string) {

        return this.sanitizer.bypassSecurityTrustHtml(template);
    }
}
