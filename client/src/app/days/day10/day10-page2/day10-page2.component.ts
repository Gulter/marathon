import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay10 } from "../../../state";

@Component({
    selector: "app-day10-page2",
    templateUrl: "./day10-page2.component.html",
    styleUrls: ["./day10-page2.component.scss"],
})
export class Day10Page2Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay10;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    get cardCanBeSelected() { return this.state.page2.selectedCards.length < 2; }

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
