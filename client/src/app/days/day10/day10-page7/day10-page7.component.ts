import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay10 } from "../../../state";

@Component({
    selector: "app-day10-page7",
    templateUrl: "./day10-page7.component.html",
    styleUrls: ["./day10-page7.component.scss"],
})
export class Day10Page7Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay10;
    @Output() prevPage = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
