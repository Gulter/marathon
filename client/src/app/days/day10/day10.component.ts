import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay10 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day10",
    templateUrl: "./day10.component.html",
    styleUrls: ["./day10.component.scss"],
})
export class Day10Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay10;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
