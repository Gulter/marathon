import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay10 } from "../../../state";

@Component({
    selector: "app-day10-page1",
    templateUrl: "./day10-page1.component.html",
    styleUrls: ["./day10-page1.component.scss"],
})
export class Day10Page1Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay10;
    @Output() prevDay = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    get cardCanBeSelected() { return this.state.page1.selectedCards.length < 2; }

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
