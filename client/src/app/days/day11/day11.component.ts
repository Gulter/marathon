import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay11 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day11",
    templateUrl: "./day11.component.html",
    styleUrls: ["./day11.component.scss"],
})
export class Day11Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay11;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
