import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay11 } from "../../../state";

@Component({
    selector: "app-day11-page4",
    templateUrl: "./day11-page4.component.html",
    styleUrls: ["./day11-page4.component.scss"],
})
export class Day11Page4Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay11;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
