import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay1 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day1",
    templateUrl: "./day1.component.html",
    styleUrls: ["./day1.component.scss"],
})
export class Day1Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay1;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
