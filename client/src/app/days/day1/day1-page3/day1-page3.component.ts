import * as E from "linq";
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay1 } from "../../../state";

@Component({
    selector: "app-day1-page3",
    templateUrl: "./day1-page3.component.html",
    styleUrls: ["./day1-page3.component.scss"],
})
export class Day1Page3Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay1;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
