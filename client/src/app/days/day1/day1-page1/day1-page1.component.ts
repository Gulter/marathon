import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay1 } from "../../../state";

@Component({
    selector: "app-day1-page1",
    templateUrl: "./day1-page1.component.html",
    styleUrls: ["./day1-page1.component.scss"],
})
export class Day1Page1Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay1;
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
