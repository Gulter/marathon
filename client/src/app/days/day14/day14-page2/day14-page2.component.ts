import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay14 } from "../../../state";

@Component({
    selector: "app-day14-page2",
    templateUrl: "./day14-page2.component.html",
    styleUrls: ["./day14-page2.component.scss"],
})
export class Day14Page2Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay14;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
