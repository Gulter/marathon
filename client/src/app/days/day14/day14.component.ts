import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay14 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day14",
    templateUrl: "./day14.component.html",
    styleUrls: ["./day14.component.scss"],
})
export class Day14Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay14;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
