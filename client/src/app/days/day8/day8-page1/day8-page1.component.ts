import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay8 } from "../../../state";

@Component({
    selector: "app-day8-page1",
    templateUrl: "./day8-page1.component.html",
    styleUrls: ["./day8-page1.component.scss"],
})
export class Day8Page1Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay8;
    @Output() prevDay = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    @Input() name: string;
    @Input() question: string;
    @Input() answer: boolean;
    @Output() answerChange = new EventEmitter<boolean>();

    get idYes() { return `${this.name}_yes`; }
    get idNo() { return `${this.name}_no`; }
    get idTotallyAgree() { return `${this.name}_TotallyAgree`; }
    get idAlmostAgree() { return `${this.name}_AlmostAgree`; }
    get idRatherAgree() { return `${this.name}_RatherAgree`; }
    get idDisagree() { return `${this.name}_Disagree`; }

    onAnswerChange(model: boolean){

        this.answer = model;
        this.answerChange.emit(model);
    }

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
