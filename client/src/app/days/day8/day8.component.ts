import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay8 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day8",
    templateUrl: "./day8.component.html",
    styleUrls: ["./day8.component.scss"],
})
export class Day8Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay8;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
