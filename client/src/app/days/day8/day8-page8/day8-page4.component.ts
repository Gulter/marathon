import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay8 } from "../../../state";

@Component({
    selector: "app-day8-page4",
    templateUrl: "./day8-page4.component.html",
    styleUrls: ["./day8-page4.component.scss"],
})
export class Day8Page4Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay8;
    @Output() prevPage = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
