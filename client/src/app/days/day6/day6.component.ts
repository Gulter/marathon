import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay6 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day6",
    templateUrl: "./day6.component.html",
    styleUrls: ["./day6.component.scss"],
})
export class Day6Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay6;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
