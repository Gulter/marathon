import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay6 } from "../../../state";

@Component({
    selector: "app-day6-page7",
    templateUrl: "./day6-page7.component.html",
    styleUrls: ["./day6-page7.component.scss"],
})
export class Day6Page7Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay6;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
