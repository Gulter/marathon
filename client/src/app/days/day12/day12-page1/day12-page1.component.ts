import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay12 } from "../../../state";

@Component({
    selector: "app-day12-page1",
    templateUrl: "./day12-page1.component.html",
    styleUrls: ["./day12-page1.component.scss"],
})
export class Day12Page1Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay12;
    @Output() prevDay = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
