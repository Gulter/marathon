import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay12 } from "../../../state";

@Component({
    selector: "app-day12-page4",
    templateUrl: "./day12-page4.component.html",
    styleUrls: ["./day12-page4.component.scss"],
})
export class Day12Page4Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay12;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
