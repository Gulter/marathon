import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay12 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day12",
    templateUrl: "./day12.component.html",
    styleUrls: ["./day12.component.scss"],
})
export class Day12Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay12;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
