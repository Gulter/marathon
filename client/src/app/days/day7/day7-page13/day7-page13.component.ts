import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay7 } from "../../../state";

@Component({
    selector: "app-day7-page13",
    templateUrl: "./day7-page13.component.html",
    styleUrls: ["./day7-page13.component.scss"],
})
export class Day7Page13Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay7;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
