import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay7 } from "../../../state";

@Component({
    selector: "app-day7-page9",
    templateUrl: "./day7-page9.component.html",
    styleUrls: ["./day7-page9.component.scss"],
})
export class Day7Page9Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay7;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    get cardCanBeSelected() { return this.state.page9.selectedCards.length < 2; }

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
