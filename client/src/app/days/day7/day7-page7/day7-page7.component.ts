import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay7 } from "../../../state";

@Component({
    selector: "app-day7-page7",
    templateUrl: "./day7-page7.component.html",
    styleUrls: ["./day7-page7.component.scss"],
})
export class Day7Page7Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay7;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    get cardCanBeSelected() { return this.state.page7.selectedCards.length < 2; }

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
