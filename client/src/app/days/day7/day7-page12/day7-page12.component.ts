import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay7 } from "../../../state";

@Component({
    selector: "app-day7-page12",
    templateUrl: "./day7-page12.component.html",
    styleUrls: ["./day7-page12.component.scss"],
})
export class Day7Page12Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay7;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
