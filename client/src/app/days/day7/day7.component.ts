import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay7 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day7",
    templateUrl: "./day7.component.html",
    styleUrls: ["./day7.component.scss"],
})
export class Day7Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay7;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
