import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay5 } from "../../../state";

@Component({
    selector: "app-day5-page6",
    templateUrl: "./day5-page6.component.html",
    styleUrls: ["./day5-page6.component.scss"],
})
export class Day5Page6Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay5;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
