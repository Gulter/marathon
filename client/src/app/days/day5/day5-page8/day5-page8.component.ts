import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay5 } from "../../../state";

@Component({
    selector: "app-day5-page8",
    templateUrl: "./day5-page8.component.html",
    styleUrls: ["./day5-page8.component.scss"],
})
export class Day5Page8Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay5;
    @Output() prevPage = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
