import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay5 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day5",
    templateUrl: "./day5.component.html",
    styleUrls: ["./day5.component.scss"],
})
export class Day5Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay5;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
