import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay5 } from "../../../state";

@Component({
    selector: "app-day5-page5",
    templateUrl: "./day5-page5.component.html",
    styleUrls: ["./day5-page5.component.scss"],
})
export class Day5Page5Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay5;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    get cardCanBeSelected() { return this.state.page5.selectedCards.length < 3; }

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
