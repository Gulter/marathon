import { OnInit } from "@angular/core";

export class BaseDayComponent implements OnInit {

    protected pageToShow: number;
    protected state: { maxPageNumber: number };

    protected _page: number;

    get currentPage() { return this._page; }
    set currentPage(value) {

        if (value < 1) this._page = 1;
        else if (value > this.state.maxPageNumber) this._page = this.state.maxPageNumber;
        else this._page = value;
    }

    ngOnInit() { this.currentPage = this.pageToShow; }

    prevPage() { this.currentPage -= 1; }
    nextPage() { this.currentPage += 1; }
}
