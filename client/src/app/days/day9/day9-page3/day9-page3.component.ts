import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay9 } from "../../../state";

@Component({
    selector: "app-day9-page3",
    templateUrl: "./day9-page3.component.html",
    styleUrls: ["./day9-page3.component.scss"],
})
export class Day9Page3Component extends BasePageComponent {

    @Input() templates: string[];
    @Input() state: StateDay9;
    @Output() prevPage = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }
}
