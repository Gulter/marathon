import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateDay9 } from "../../state";
import { BaseDayComponent } from "../baseDayComponent";

@Component({
    selector: "app-day9",
    templateUrl: "./day9.component.html",
    styleUrls: ["./day9.component.scss"],
})
export class Day9Component extends BaseDayComponent {

    @Input() pageToShow: number;
    @Input() templates: string[];
    @Input() state: StateDay9;
    @Output() prevDay = new EventEmitter();
    @Output() nextDay = new EventEmitter();

    constructor(
    ) {
        super();
    }
}
