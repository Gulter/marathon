import * as E from "linq";
import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { YouTubePlayerService } from "../../../you-tube-player.service";
import { DomSanitizer } from "@angular/platform-browser";
import { BasePageComponent } from "../../basePageComponent";
import { StateDay9, Video } from "../../../state";

@Component({
    selector: "app-day9-page1",
    templateUrl: "./day9-page1.component.html",
    styleUrls: ["./day9-page1.component.scss"],
})
export class Day9Page1Component extends BasePageComponent implements OnInit {

    @Input() templates: string[];
    @Input() state: StateDay9;
    @Output() prevDay = new EventEmitter();
    @Output() nextPage = new EventEmitter();

    constructor(
        private youTubePlayerService: YouTubePlayerService,
        protected sanitizer: DomSanitizer,
    ) {
        super(sanitizer);
    }

    video1: Video;
    video2: Video;
    video3: Video;

    ngOnInit() {

        this.video1 = this.getVideo(1);
        this.video2 = this.getVideo(2);
        this.video3 = this.getVideo(3);

        this.startVideo(1);
        // this.startVideo(2);
        // this.startVideo(3);
    }

    getVideo(videoNumber: number) {

        return E.from(this.state.page1.videos)
            .where(i => i.videoNumber == videoNumber)
            .firstOrDefault(null, {} as Video);
    }

    startVideo(videoNumber: number) {

        const video = this.getVideo(videoNumber);

        if (video.name) {

            this.youTubePlayerService.video = video.name;
            this.youTubePlayerService.init(`player${videoNumber}`, () => this.videoViewed(video));
        }
    }

    videoViewed(video: Video) {

        video.viewed = true;
        console.log('videoViewed!!!');
    }
}
