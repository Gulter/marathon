export class State {

    currentDay: number;
    currentPage: number;
    readonly minAllowedDay = 1;
    readonly maxAllowedDay = 21;
    templates: Array<string[]>;

    day1: StateDay1;
    day2: StateDay2;
    day3: StateDay3;
    day4: StateDay4;
    day5: StateDay5;
    day6: StateDay6;
    day7: StateDay7;
    day8: StateDay8;
    day9: StateDay9;
    day10: StateDay10;
    day11: StateDay11;
    day12: StateDay12;
    day13: StateDay13;
    day14: StateDay14;
}

export interface Card {
    day: number,
    page: number,
    cardNumber: number,
    selected?: boolean,
    turned?: boolean,
    clicked?: boolean,
}

export interface Video {
    day: number,
    page: number,
    videoNumber: number,
    name: string,
    viewed: boolean,
}

export interface StateDay1 {

    maxPageNumber: number;
    page3: {
        note1: string,
        note2: string,
        note3: string,
    },
    page4: {
        note1: string,
    },
    page5: {
        answer1: boolean,
        answer2: boolean,
        answer3: boolean,
        answer4: boolean,
        answer5: boolean,
        answer6: boolean,
        answer7: boolean,
        answer8: boolean,
    },
}

export interface StateDay2 {

    maxPageNumber: number;
    page2: {
        note1: string,
    },
    page3: {
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
        note10: string,
        note11: string,
        note12: string,
    },
    page4: {
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
    },
    page5: {
        answer1: boolean,
        answer2: boolean,
        answer3: boolean,
        answer4: boolean,
        answer5: boolean,
        answer6: boolean,
        answer7: boolean,
        answer8: boolean,
    },
}

export interface StateDay3 {

    maxPageNumber: number;
    page3: {
        note1: string,
    },
    page4: {
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
    },
    page6: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page7: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page8: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page9: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page10: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page11: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page12: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page13: {
        note1: string,
    },
    page14: {
        answer1: boolean,
        answer2: boolean,
        answer3: boolean,
        answer4: boolean,
        answer5: boolean,
        answer6: boolean,
        answer7: boolean,
        answer8: boolean,
    },
}

export interface StateDay4 {

    maxPageNumber: number;
    page2: {
        note1: string,
    },
    page3: {
        note1: string,
    },
    page4: {
        note1: string,
    },
    page5: {
        note1: string,
        note2: string,
    },
    page6: {
        note1: string,
    },
    page7: {
        note1: string,
    },
    page8: {
        note1: string,
    },
    page9: {
        answer1: boolean,
        answer2: boolean,
        answer3: boolean,
        answer4: boolean,
        answer5: boolean,
        answer6: boolean,
        answer7: boolean,
        answer8: boolean,
    },
}

export interface StateDay5 {

    maxPageNumber: number;
    page1: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page2: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page3: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page4: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page5: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
    },
    page6: {
        note1: string,
    },
    page7: {
        answer1: boolean,
        answer2: boolean,
        answer3: boolean,
        answer4: boolean,
        answer5: boolean,
        answer6: boolean,
        answer7: boolean,
        answer8: boolean,
    },
}

export interface StateDay6 {

    maxPageNumber: number;
    page1: {
        note1: string,
    },
    page2: {
        note1: string,
    },
    page3: {
        note1: string,
    },
    page5: {
        note1: string,
        note2: string,
    },
    page6: {
        note1: string,
    },
    page7: {
        note1: string,
        note2: string,
    },
    page8: {
        note1: string,
    },
}

export interface StateDay7 {

    maxPageNumber: number;
    page1: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
        note10: string,
        note11: string,
        note12: string,
    },
    page3: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
        note10: string,
        note11: string,
        note12: string,
    },
    page4: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
        note10: string,
        note11: string,
        note12: string,
    },
    page5: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
        note10: string,
        note11: string,
        note12: string,
    },
    page6: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
        note10: string,
        note11: string,
        note12: string,
    },
    page7: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
        note10: string,
        note11: string,
        note12: string,
    },
    page8: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
        note10: string,
        note11: string,
        note12: string,
    },
    page9: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
        note7: string,
        note8: string,
        note9: string,
        note10: string,
        note11: string,
        note12: string,
    },
    page10: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
        note4: string,
        note5: string,
        note6: string,
    },
    page11: {
        note1: string,
    },
    page12: {
        note1: string,
        note2: string,
        note3: string,
        note4: string,
    },
    page14: {
        note1: string,
        note2: string,
    },
}

export interface StateDay8 {

    maxPageNumber: number;
    page3: {
        note1: string,
    },
}

export interface StateDay9 {

    maxPageNumber: number;
    page1: {
        videos: Video[],
        note1: string,
        note2: string,
        note3: string,
    },
    page2: {
        note1: string,
    },
    page3: {
        note1: string,
    },
    page4: {
        note1: string,
    },
}

export interface StateDay10 {

    maxPageNumber: number;
    page1: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
    },
    page2: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
    },
    page3: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
    },
    page4: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
    },
    page5: {
        cards: Card[],
        selectedCards: Card[],
        note1: string,
        note2: string,
        note3: string,
    },
    page6: {
        note1: string,
        note2: string,
        note3: string,
        note4: string,
    },
    page7: {
        note1: string,
    },
}

export interface StateDay11 {

    maxPageNumber: number;
    page5: {
        note1: string,
    },
    page6: {
        note1: string,
        note2: string,
    },
    page7: {
        note1: string,
        note2: string,
    },
}

export interface StateDay12 {

    maxPageNumber: number;
    page1: {
        note1: string,
    },
    page2: {
        note1: string,
    },
    page3: {
        note1: string,
    },
    page5: {
        note1: string,
        note2: string,
    },
    page6: {
        note1: string,
    },
    page7: {
        note1: string,
        note2: string,
    },
}

export interface StateDay13 {

    maxPageNumber: number;
    page1: {
        note1: string,
    },
    page2: {
        note1: string,
    },
    page3: {
        note1: string,
    },
}

export interface StateDay14 {

    maxPageNumber: number;
    page1: {
        note1: string,
    },
    page2: {
        note1: string,
    },
    page3: {
        note1: string,
    },
    page5: {
        note1: string,
        note2: string,
    },
}
