import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
    title = "Marathon";

    constructor(
        private http: HttpClient,
    ) {
    }

    ngOnInit() {

        // this.http.get("http://localhost:3000/users")
        //     .pipe(
        //         tap(i => {
        //             console.log(i);
        //         }),
        //     )
        //     .subscribe(i => {
        //         console.log(1111, i);
        //     });
    }
}
