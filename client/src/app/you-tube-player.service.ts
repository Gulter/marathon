import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class YouTubePlayerService {

    public YT: any;
    public video: any;
    public playerObj: any;
    public playerId: string;
    public reframed: Boolean = false;

    isRestricted = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

    init(playerId: string, onVideoEnded: () => void) {

        this.playerId = playerId;

        if (window["YT"]) return this.onYouTubeIframeAPIReady(onVideoEnded);

        const tag = document.createElement("script");
        tag.src = "https://www.youtube.com/iframe_api";

        const firstScriptTag = document.getElementsByTagName("script")[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        window["onYouTubeIframeAPIReady"] = () => this.onYouTubeIframeAPIReady(onVideoEnded);
    }

    onYouTubeIframeAPIReady(onVideoEnded: () => void) {

        this.reframed = false;
        this.playerObj = new window["YT"].Player(this.playerId, {
            videoId: this.video,
            playerVars: {
                autoplay: 0,
                modestbranding: 1,
                controls: 1,
                disablekb: 1,
                rel: 0,
                showinfo: 0,
                fs: 0,
                playsinline: 1,
            },
            events: {
                "onStateChange": event => this.onPlayerStateChange(event, onVideoEnded),
                "onError": event => this.onPlayerError(event),
            }
        });
    }

    onPlayerStateChange(event, onVideoEnded: () => void) {

        switch (event.data) {
            case window["YT"].PlayerState.PLAYING:

                if (this.cleanTime() == 0) {

                    console.log(`started ${this.cleanTime()}`);
                }
                else {

                    console.log(`playing ${this.cleanTime()}`)
                }
                break;

            case window["YT"].PlayerState.PAUSED:

                if (this.playerObj.getDuration() - this.playerObj.getCurrentTime() != 0) {

                    console.log(`paused @ ${this.cleanTime()}`);
                }
                break;

            case window["YT"].PlayerState.ENDED:

                onVideoEnded();
                console.log("ended ");
                break;
        }
    };

    cleanTime() {

        return Math.round(this.playerObj.getCurrentTime())
    };

    onPlayerError(event) {

        switch (event.data) {
            case 2:
                console.log("" + this.video);
                break;

            case 100:
                break;

            case 101 || 150:
                break;
        }
    };
}
