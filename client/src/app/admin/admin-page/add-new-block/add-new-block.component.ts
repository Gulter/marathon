import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Page } from 'src/app/services/admin/Page';
import { FormBuilder } from '@angular/forms';
import { BlockTypeEnum } from 'src/app/services/admin/BlockType';
import { Block } from 'src/app/services/admin/Block';

@Component({
    selector: "app-add-new-block",
    templateUrl: "./add-new-block.component.html",
    styleUrls: ["./add-new-block.component.scss"]
})
export class AddNewBlockComponent implements OnInit {

    @Input() page: Page;
    @Output() saveBlock = new EventEmitter<Block>();

    constructor(
        private fb: FormBuilder,
    ) {}

    blockTypes = [
        { code: BlockTypeEnum.TEXT, name: "Text" },
        // { code: BlockTypeEnum.PICTURE, name: "Picture" },
        // { code: BlockTypeEnum.LINK, name: "Link" },
        // { code: BlockTypeEnum.VIDEO, name: "Video" },
        // { code: BlockTypeEnum.AUDIO, name: "Audio" },
        { code: BlockTypeEnum.TEXTAREA, name: "Textarea" },
    ];

    get blockType() { return this.blockForm.get("blockType").value; };

    BlockType = BlockTypeEnum;

    blockForm = this.fb.group({
        blockType: [null],
        text: [null],
        // url: [null],
    });

    ngOnInit() {}

    saveBlockClick() {

        const data = this.blockForm.value;

        this.saveBlock.emit({
            pageId: this.page.id,
            ...data
        });
    }
}
