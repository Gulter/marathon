import * as E from "linq";
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from "@angular/core";
import { Page } from "src/app/services/admin/Page";
import { FormBuilder } from "@angular/forms";
import { Block } from 'src/app/services/admin/Block';
import { BlockTypeEnum } from 'src/app/services/admin/BlockType';
import { BlocksChangePriorityWithInterface } from 'src/app/storage/admin/admin.actions';

@Component({
    selector: "app-edit-page",
    templateUrl: "./edit-page.component.html",
    styleUrls: ["./edit-page.component.scss"]
})
export class EditPageComponent implements OnInit, OnChanges {

    @Input() page: Page;
    @Output() saveBlock = new EventEmitter<Block>();
    @Output() removeBlock = new EventEmitter<Block>();
    @Output() changePriorityWith = new EventEmitter<BlocksChangePriorityWithInterface>();

    addMode: boolean;
    blocks: Block[];

    constructor(
        // private fb: FormBuilder
    ) {}

    ngOnInit() {}

    ngOnChanges() {

        if (this.page) {

            this.blocks = E.from(this.page.blocks)
                .orderBy(i => i.priority)
                .toArray();
        }
    }

    addNewBlockClick() {

        this.addMode = true;
    }

    getBlockTypeName(blockType: BlockTypeEnum) {

        const map = {
            [BlockTypeEnum.TEXT]: "Text",
            [BlockTypeEnum.PICTURE]: "Picture",
            [BlockTypeEnum.LINK]: "Link",
            [BlockTypeEnum.VIDEO]: "Video",
            [BlockTypeEnum.AUDIO]: "Audio",
            [BlockTypeEnum.TEXTAREA]: "Textarea",
        };

        return map[blockType];
    }

    removeBlockClick(block: Block) {

        this.removeBlock.emit(block);
    }

    changePriorityWithClick(index: number, otherIndex: number) {

        this.changePriorityWith.emit({
            id: this.blocks[index].id,
            otherId: this.blocks[otherIndex].id,
            pageId: this.page.id,
        });
    }
}
