import { Component, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { IStore, getAdminState } from "src/app/storage/store";
import { map } from 'rxjs/operators';
import { GetPageAction, AddBlockAction, RemoveBlockAction, BlocksChangePriorityWithInterface, BlocksChangePriorityWithAction } from 'src/app/storage/admin/admin.actions';
import { ActivatedRoute } from '@angular/router';
import { Block } from 'src/app/services/admin/Block';

@Component({
    selector: "app-admin-page",
    templateUrl: "./admin-page.component.html",
    styleUrls: ["./admin-page.component.scss"]
})
export class AdminPageComponent implements OnInit {

    page$ = this.store.pipe(
        select(getAdminState),
        map(i => i.selectedPage),
    );

    constructor(
        private store: Store<IStore>,
        private activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit() {

        const id = this.activatedRoute.snapshot.paramMap.get("id");

        this.store.dispatch(GetPageAction({ id: +id }));
    }

    saveBlock(block: Block) {

        this.store.dispatch(AddBlockAction({ block }));
    }

    removeBlock(block: Block) {

        this.store.dispatch(RemoveBlockAction({ block }));
    }

    changePriorityWith(data: BlocksChangePriorityWithInterface) {

        this.store.dispatch(BlocksChangePriorityWithAction(data));
    }
}
