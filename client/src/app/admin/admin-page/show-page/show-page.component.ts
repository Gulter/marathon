import * as E from "linq";
import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { Page } from "src/app/services/admin/Page";
import { DomSanitizer } from "@angular/platform-browser";
import { BlockTypeEnum } from "src/app/services/admin/BlockType";
import { Block } from 'src/app/services/admin/Block';

@Component({
    selector: "app-show-page",
    templateUrl: "./show-page.component.html",
    styleUrls: ["./show-page.component.scss"]
})
export class ShowPageComponent implements OnInit, OnChanges {

    @Input() page: Page;

    blocks: Block[];
    BlockType = BlockTypeEnum;

    constructor(
        protected sanitizer: DomSanitizer,
    ) {}

    ngOnInit() {}

    ngOnChanges() {

        if (this.page) {

            this.blocks = E.from(this.page.blocks)
                .orderBy(i => i.priority)
                .toArray();
        }
    }

    sanitizeTemplate(template: string) {

        return this.sanitizer.bypassSecurityTrustHtml(template);
    }
}
