import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { Day } from 'src/app/services/admin/Day';

@Component({
    selector: "app-add-page-modal",
    templateUrl: "./add-page-modal.component.html",
    styleUrls: ["./add-page-modal.component.scss"]
})
export class AddPageModalComponent implements OnInit {

    @Input() day: Day;

    constructor(
        private activeModal: NgbActiveModal,
        private fb: FormBuilder
    ) {}

    dayForm = this.fb.group({
        title: [null],
    });

    ngOnInit() {}

    save() {

        const data = this.dayForm.value;

        this.activeModal.close({
            ...data,
            dayId: this.day.id,
        });
    }
}
