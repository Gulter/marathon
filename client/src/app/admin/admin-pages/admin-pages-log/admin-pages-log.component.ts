import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { Day } from "src/app/services/admin/Day";
import { Expandable } from "src/app/types/expandable";

@Component({
    selector: "app-admin-pages-log",
    templateUrl: "./admin-pages-log.component.html",
    styleUrls: ["./admin-pages-log.component.scss"]
})
export class AdminPagesLogComponent implements OnInit {

    @Input() days: Expandable<Day>[];
    @Output() addDay = new EventEmitter();
    @Output() addPage = new EventEmitter<Day>();

    constructor() {}

    ngOnInit() {}

    addDayClick() {

        this.addDay.emit();
    }

    addPageClick(day: Day) {

        this.addPage.emit(day);
    }
}
