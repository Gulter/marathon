import { Component, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { IStore, getAdminState } from "src/app/storage/store";
import { map } from 'rxjs/operators';
import { GetDaysAction, AddDayAction, AddPageAction } from 'src/app/storage/admin/admin.actions';
import { Day } from 'src/app/services/admin/Day';

@Component({
    selector: "app-admin-pages",
    templateUrl: "./admin-pages.component.html",
    styleUrls: ["./admin-pages.component.scss"]
})
export class AdminPagesComponent implements OnInit {

    days$ = this.store.pipe(
        select(getAdminState),
        map(i => i.days),
    );

    constructor(
        private store: Store<IStore>
    ) {}

    ngOnInit() {

        this.store.dispatch(GetDaysAction());
    }

    addDay() {

        this.store.dispatch(AddDayAction());
    }

    addPage(day: Day) {

        this.store.dispatch(AddPageAction({ day }));
    }
}
