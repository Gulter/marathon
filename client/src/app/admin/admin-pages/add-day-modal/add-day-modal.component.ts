import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder } from "@angular/forms";

@Component({
    selector: "app-add-day-modal",
    templateUrl: "./add-day-modal.component.html",
    styleUrls: ["./add-day-modal.component.scss"]
})
export class AddDayModalComponent implements OnInit {

    constructor(
        private activeModal: NgbActiveModal,
        private fb: FormBuilder
    ) {}

    dayForm = this.fb.group({
        title: [null]
    });

    ngOnInit() {}

    save() {
        const data = this.dayForm.value;
        this.activeModal.close(data);
    }
}
