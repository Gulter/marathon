import { Component, OnInit } from "@angular/core";
import { initState } from "../initState";

@Component({
    selector: "app-days-view",
    templateUrl: "./days-view.component.html"
})
export class DaysViewComponent implements OnInit {

    constructor() {}

    state = initState();

    ngOnInit() {}

    getTemplates(i: number) {

        return i >= this.state.minAllowedDay && i <= this.state.maxAllowedDay
            ? this.state.templates[i - 1]
            : [];
    }

    prevDay() {

        if (this.state.currentDay > 1) this.state.currentDay -= 1;

        const day = `day${this.state.currentDay}`;
        this.state.currentPage =
            (this.state[day] && this.state[day].maxPageNumber) || 1;

        this.printState();
    }

    nextDay() {

        this.state.currentDay += 1;
        this.state.currentPage = 1;
        this.printState();
    }

    printState() {

        console.log("this.state =", this.state);
    }

    incDay() {

        if (this.state.currentDay < 21) this.state.currentDay += 1;
    }

    decDay() {

        if (this.state.currentDay > 1) this.state.currentDay -= 1;
    }
}
