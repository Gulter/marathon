import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { StoreRouterConnectingModule } from "@ngrx/router-store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { NgbModule, NgbModalModule } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";

import { AdminReducer } from "./storage/admin/admin.reducer";
import { AdminEffects } from "./storage/admin/admin.effects";

import { AppComponent } from "./app.component";

import { AdminPagesComponent } from "./admin/admin-pages/admin-pages.component";
import { AdminPageComponent } from "./admin/admin-page/admin-page.component";
import { AdminPagesLogComponent } from "./admin/admin-pages/admin-pages-log/admin-pages-log.component";
import { EditPageComponent } from "./admin/admin-page/edit-page/edit-page.component";
import { ShowPageComponent } from "./admin/admin-page/show-page/show-page.component";
import { AddDayModalComponent } from "./admin/admin-pages/add-day-modal/add-day-modal.component";
import { AddPageModalComponent } from "./admin/admin-pages/add-page-modal/add-page-modal.component";
import { AddNewBlockComponent } from "./admin/admin-page/add-new-block/add-new-block.component";

import { TwoColumnsGridComponent } from "./shared/two-columns-grid/two-columns-grid.component";
import { HeaderComponent } from "./shared/header/header.component";
import { NavigationComponent } from "./shared/navigation/navigation.component";
import { CardComponent } from "./shared/card/card.component";
import { CardAndCommentComponent } from './shared/card-and-comment/card-and-comment.component';
import { QuestionComponent } from "./shared/question/question.component";
import { BlockPlaceDirective } from "./shared/block-place/block-place.directive";

import { DaysViewComponent } from "./days-view/days-view.component";

import { Day1Component } from "./days/day1/day1.component";
import { Day1Page1Component } from "./days/day1/day1-page1/day1-page1.component";
import { Day1Page2Component } from "./days/day1/day1-page2/day1-page2.component";
import { Day1Page3Component } from "./days/day1/day1-page3/day1-page3.component";
import { Day1Page4Component } from "./days/day1/day1-page4/day1-page4.component";
import { Day1Page5Component } from "./days/day1/day1-page5/day1-page5.component";
import { Day1Page6Component } from "./days/day1/day1-page6/day1-page6.component";

import { Day2Component } from "./days/day2/day2.component";
import { Day2Page1Component } from "./days/day2/day2-page1/day2-page1.component";
import { Day2Page2Component } from "./days/day2/day2-page2/day2-page2.component";
import { Day2Page3Component } from "./days/day2/day2-page3/day2-page3.component";
import { Day2Page4Component } from "./days/day2/day2-page4/day2-page4.component";
import { Day2Page5Component } from "./days/day2/day2-page5/day2-page5.component";
import { Day2Page6Component } from "./days/day2/day2-page6/day2-page6.component";

import { Day3Component } from "./days/day3/day3.component";
import { Day3Page1Component } from "./days/day3/day3-page1/day3-page1.component";
import { Day3Page2Component } from "./days/day3/day3-page2/day3-page2.component";
import { Day3Page3Component } from "./days/day3/day3-page3/day3-page3.component";
import { Day3Page4Component } from "./days/day3/day3-page4/day3-page4.component";
import { Day3Page5Component } from "./days/day3/day3-page5/day3-page5.component";
import { Day3Page6Component } from "./days/day3/day3-page6/day3-page6.component";
import { Day3Page7Component } from "./days/day3/day3-page7/day3-page7.component";
import { Day3Page8Component } from "./days/day3/day3-page8/day3-page8.component";
import { Day3Page9Component } from "./days/day3/day3-page9/day3-page9.component";
import { Day3Page10Component } from "./days/day3/day3-page10/day3-page10.component";
import { Day3Page11Component } from "./days/day3/day3-page11/day3-page11.component";
import { Day3Page12Component } from "./days/day3/day3-page12/day3-page12.component";
import { Day3Page13Component } from "./days/day3/day3-page13/day3-page13.component";
import { Day3Page14Component } from "./days/day3/day3-page14/day3-page14.component";
import { Day3Page15Component } from "./days/day3/day3-page15/day3-page15.component";

import { Day4Component } from "./days/day4/day4.component";
import { Day4Page1Component } from "./days/day4/day4-page1/day4-page1.component";
import { Day4Page2Component } from "./days/day4/day4-page2/day4-page2.component";
import { Day4Page3Component } from "./days/day4/day4-page3/day4-page3.component";
import { Day4Page4Component } from "./days/day4/day4-page4/day4-page4.component";
import { Day4Page5Component } from "./days/day4/day4-page5/day4-page5.component";
import { Day4Page6Component } from "./days/day4/day4-page6/day4-page6.component";
import { Day4Page7Component } from "./days/day4/day4-page7/day4-page7.component";
import { Day4Page8Component } from "./days/day4/day4-page8/day4-page8.component";
import { Day4Page9Component } from "./days/day4/day4-page9/day4-page9.component";
import { Day4Page10Component } from "./days/day4/day4-page10/day4-page10.component";

import { Day5Component } from "./days/day5/day5.component";
import { Day5Page1Component } from "./days/day5/day5-page1/day5-page1.component";
import { Day5Page2Component } from "./days/day5/day5-page2/day5-page2.component";
import { Day5Page3Component } from "./days/day5/day5-page3/day5-page3.component";
import { Day5Page4Component } from "./days/day5/day5-page4/day5-page4.component";
import { Day5Page5Component } from "./days/day5/day5-page5/day5-page5.component";
import { Day5Page6Component } from "./days/day5/day5-page6/day5-page6.component";
import { Day5Page7Component } from "./days/day5/day5-page7/day5-page7.component";
import { Day5Page8Component } from "./days/day5/day5-page8/day5-page8.component";

import { Day6Component } from "./days/day6/day6.component";
import { Day6Page1Component } from "./days/day6/day6-page1/day6-page1.component";
import { Day6Page2Component } from "./days/day6/day6-page2/day6-page2.component";
import { Day6Page3Component } from "./days/day6/day6-page3/day6-page3.component";
import { Day6Page4Component } from "./days/day6/day6-page4/day6-page4.component";
import { Day6Page5Component } from "./days/day6/day6-page5/day6-page5.component";
import { Day6Page6Component } from "./days/day6/day6-page6/day6-page6.component";
import { Day6Page7Component } from "./days/day6/day6-page7/day6-page7.component";
import { Day6Page8Component } from "./days/day6/day6-page8/day6-page8.component";

import { Day7Component } from "./days/day7/day7.component";
import { Day7Page1Component } from "./days/day7/day7-page1/day7-page1.component";
import { Day7Page2Component } from "./days/day7/day7-page2/day7-page2.component";
import { Day7Page3Component } from "./days/day7/day7-page3/day7-page3.component";
import { Day7Page4Component } from "./days/day7/day7-page4/day7-page4.component";
import { Day7Page5Component } from "./days/day7/day7-page5/day7-page5.component";
import { Day7Page6Component } from "./days/day7/day7-page6/day7-page6.component";
import { Day7Page7Component } from "./days/day7/day7-page7/day7-page7.component";
import { Day7Page8Component } from "./days/day7/day7-page8/day7-page8.component";
import { Day7Page9Component } from "./days/day7/day7-page9/day7-page9.component";
import { Day7Page10Component } from "./days/day7/day7-page10/day7-page10.component";
import { Day7Page11Component } from "./days/day7/day7-page11/day7-page11.component";
import { Day7Page12Component } from "./days/day7/day7-page12/day7-page12.component";
import { Day7Page13Component } from "./days/day7/day7-page13/day7-page13.component";
import { Day7Page14Component } from "./days/day7/day7-page14/day7-page14.component";

import { Day8Component } from "./days/day8/day8.component";
import { Day8Page1Component } from "./days/day8/day8-page1/day8-page1.component";
import { Day8Page2Component } from "./days/day8/day8-page2/day8-page2.component";
import { Day8Page3Component } from "./days/day8/day8-page3/day8-page3.component";
import { Day8Page4Component } from "./days/day8/day8-page8/day8-page4.component";

import { Day9Component } from "./days/day9/day9.component";
import { Day9Page1Component } from "./days/day9/day9-page1/day9-page1.component";
import { Day9Page2Component } from "./days/day9/day9-page2/day9-page2.component";
import { Day9Page3Component } from "./days/day9/day9-page3/day9-page3.component";
import { Day9Page4Component } from "./days/day9/day9-page4/day9-page4.component";

import { Day10Component } from "./days/day10/day10.component";
import { Day10Page1Component } from "./days/day10/day10-page1/day10-page1.component";
import { Day10Page2Component } from "./days/day10/day10-page2/day10-page2.component";
import { Day10Page3Component } from "./days/day10/day10-page3/day10-page3.component";
import { Day10Page4Component } from "./days/day10/day10-page4/day10-page4.component";
import { Day10Page5Component } from "./days/day10/day10-page5/day10-page5.component";
import { Day10Page6Component } from "./days/day10/day10-page6/day10-page6.component";
import { Day10Page7Component } from "./days/day10/day10-page7/day10-page7.component";

import { Day11Component } from "./days/day11/day11.component";
import { Day11Page1Component } from "./days/day11/day11-page1/day11-page1.component";
import { Day11Page2Component } from "./days/day11/day11-page2/day11-page2.component";
import { Day11Page3Component } from "./days/day11/day11-page3/day11-page3.component";
import { Day11Page4Component } from "./days/day11/day11-page4/day11-page4.component";
import { Day11Page5Component } from "./days/day11/day11-page5/day11-page5.component";
import { Day11Page6Component } from "./days/day11/day11-page6/day11-page6.component";
import { Day11Page7Component } from "./days/day11/day11-page7/day11-page7.component";
import { ConfirmDialogModalComponent } from './services/dialog-services/confirm-dialog-modal/confirm-dialog-modal.component';

import { Day12Component } from "./days/day12/day12.component";
import { Day12Page1Component } from "./days/day12/day12-page1/day12-page1.component";
import { Day12Page2Component } from "./days/day12/day12-page2/day12-page2.component";
import { Day12Page3Component } from "./days/day12/day12-page3/day12-page3.component";
import { Day12Page4Component } from "./days/day12/day12-page4/day12-page4.component";
import { Day12Page5Component } from "./days/day12/day12-page5/day12-page5.component";
import { Day12Page6Component } from "./days/day12/day12-page6/day12-page6.component";
import { Day12Page7Component } from "./days/day12/day12-page7/day12-page7.component";
import { Day12Page8Component } from "./days/day12/day12-page8/day12-page8.component";
import { Day12Page9Component } from "./days/day12/day12-page9/day12-page9.component";

import { Day13Component } from "./days/day13/day13.component";
import { Day13Page1Component } from "./days/day13/day13-page1/day13-page1.component";
import { Day13Page2Component } from "./days/day13/day13-page2/day13-page2.component";
import { Day13Page3Component } from "./days/day13/day13-page3/day13-page3.component";
import { Day13Page4Component } from "./days/day13/day13-page4/day13-page4.component";
import { Day13Page5Component } from "./days/day13/day13-page5/day13-page5.component";

import { Day14Component } from "./days/day14/day14.component";
import { Day14Page1Component } from "./days/day14/day14-page1/day14-page1.component";
import { Day14Page2Component } from "./days/day14/day14-page2/day14-page2.component";
import { Day14Page3Component } from "./days/day14/day14-page3/day14-page3.component";
import { Day14Page4Component } from "./days/day14/day14-page4/day14-page4.component";
import { Day14Page5Component } from "./days/day14/day14-page5/day14-page5.component";
import { Day14Page6Component } from "./days/day14/day14-page6/day14-page6.component";

@NgModule({
    declarations: [
        AppComponent,

        AdminPagesComponent,
        TwoColumnsGridComponent,
        AdminPageComponent,
        AdminPagesLogComponent,
        EditPageComponent,
        ShowPageComponent,

        HeaderComponent,
        CardComponent,
        CardAndCommentComponent,
        QuestionComponent,

        Day1Component,
        Day1Page1Component,
        Day1Page2Component,
        Day1Page3Component,
        Day1Page4Component,
        Day1Page5Component,
        Day1Page6Component,

        Day2Component,
        Day2Page1Component,
        Day2Page2Component,
        Day2Page3Component,
        Day2Page4Component,
        Day2Page5Component,
        Day2Page6Component,

        Day3Component,
        Day3Page1Component,
        Day3Page2Component,
        Day3Page3Component,
        Day3Page4Component,
        Day3Page5Component,
        Day3Page6Component,
        Day3Page7Component,
        Day3Page8Component,
        Day3Page9Component,
        Day3Page10Component,
        Day3Page11Component,
        Day3Page12Component,
        Day3Page13Component,
        Day3Page14Component,
        Day3Page15Component,

        Day4Component,
        Day4Page1Component,
        Day4Page2Component,
        Day4Page3Component,
        Day4Page4Component,
        Day4Page5Component,
        Day4Page6Component,
        Day4Page7Component,
        Day4Page8Component,
        Day4Page9Component,
        Day4Page10Component,

        Day5Component,
        Day5Page1Component,
        Day5Page2Component,
        Day5Page3Component,
        Day5Page4Component,
        Day5Page5Component,
        Day5Page6Component,
        Day5Page7Component,
        Day5Page8Component,

        Day6Component,
        Day6Page1Component,
        Day6Page2Component,
        Day6Page3Component,
        Day6Page4Component,
        Day6Page5Component,
        Day6Page6Component,
        Day6Page7Component,
        Day6Page8Component,

        Day7Component,
        Day7Page1Component,
        Day7Page2Component,
        Day7Page3Component,
        Day7Page4Component,
        Day7Page5Component,
        Day7Page6Component,
        Day7Page7Component,
        Day7Page8Component,
        Day7Page9Component,
        Day7Page10Component,
        Day7Page11Component,
        Day7Page12Component,
        Day7Page13Component,
        Day7Page14Component,

        Day8Component,
        Day8Page1Component,
        Day8Page2Component,
        Day8Page3Component,
        Day8Page4Component,

        Day9Component,
        Day9Page1Component,
        Day9Page2Component,
        Day9Page3Component,
        Day9Page4Component,

        Day10Component,
        Day10Page1Component,
        Day10Page2Component,
        Day10Page3Component,
        Day10Page4Component,
        Day10Page5Component,
        Day10Page6Component,
        Day10Page7Component,

        Day11Component,
        Day11Page1Component,
        Day11Page2Component,
        Day11Page3Component,
        Day11Page4Component,
        Day11Page5Component,
        Day11Page6Component,
        Day11Page7Component,
        NavigationComponent,

        Day12Component,
        Day12Page1Component,
        Day12Page2Component,
        Day12Page3Component,
        Day12Page4Component,
        Day12Page5Component,
        Day12Page6Component,
        Day12Page7Component,
        Day12Page8Component,
        Day12Page9Component,

        Day13Component,
        Day13Page1Component,
        Day13Page2Component,
        Day13Page3Component,
        Day13Page4Component,
        Day13Page5Component,

        Day14Component,
        Day14Page1Component,
        Day14Page2Component,
        Day14Page3Component,
        Day14Page4Component,
        Day14Page5Component,
        Day14Page6Component,

        DaysViewComponent,
        AddDayModalComponent,
        AddPageModalComponent,
        AddNewBlockComponent,
        BlockPlaceDirective,
        ConfirmDialogModalComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NgbModalModule,
        NgSelectModule,
        EffectsModule.forRoot([
            AdminEffects,
        ]),
        StoreModule.forRoot({
            admin: AdminReducer,
        }),
        StoreRouterConnectingModule.forRoot(),
        StoreDevtoolsModule.instrument({ maxAge: 25 })
    ],
    providers: [],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents: [
        AddDayModalComponent,
        AddPageModalComponent,
        ConfirmDialogModalComponent,
    ]
})
export class AppModule {
}
