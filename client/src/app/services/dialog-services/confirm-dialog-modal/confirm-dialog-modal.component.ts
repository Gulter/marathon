import { Component, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: "app-confirm-dialog-modal",
    templateUrl: "./confirm-dialog-modal.component.html",
    styleUrls: ["./confirm-dialog-modal.component.scss"]
})
export class ConfirmDialogModalComponent {

    @Input() title: string;
    @Input() text: string;

    constructor(
        public activeModal: NgbActiveModal,
    ) {}
}
