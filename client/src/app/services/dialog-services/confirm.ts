import { Injectable } from "@angular/core";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogModalComponent } from './confirm-dialog-modal/confirm-dialog-modal.component';

@Injectable({ providedIn: "root" })
export class DialogService {

    constructor(
        private modal: NgbModal,
    ) {}

    confirm$({
        title = "Confirmation",
        text = "Are you sure?",
    }: {
        title?: string,
        text?: string,
    }) {

        const modal = this.modal.open(ConfirmDialogModalComponent, {
            size: "sm",
        });

        modal.componentInstance.title = title;
        modal.componentInstance.text = text;

        return modal.result;
    }
}