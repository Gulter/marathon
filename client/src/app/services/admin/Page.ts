import { Block } from "./Block";

export interface Page {
    id: number;
    title: string;
    blocks: Block[];
    dayId: number;
}
