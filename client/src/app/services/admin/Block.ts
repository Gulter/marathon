import { BlockTypeEnum } from "../../services/admin/BlockType";
import { Page } from "./Page";

export interface Block {
    id: number;
    text: string;
    url: string;
    blockType: BlockTypeEnum;
    page: Page;
    pageId: number;
    priority: number;
}
