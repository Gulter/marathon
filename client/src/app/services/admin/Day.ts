import { Page } from './Page';

export interface Day {
    id: number;
    title: string;
    pages: Page[];
}
