export enum BlockTypeEnum {
    TEXT = 1,
    PICTURE = 2,
    LINK = 3,
    VIDEO = 4,
    AUDIO = 5,
    TEXTAREA = 6,
}
