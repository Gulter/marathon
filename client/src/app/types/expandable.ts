export type Expandable<T> = T & { collapsed: boolean };
